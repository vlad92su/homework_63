<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FollowersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $followers = [
            [
                'follower_id' => 1,
                'followed_id' => 2,
            ],
            [
                'follower_id' => 1,
                'followed_id' => 3,
            ],
            [
                'follower_id' => 2,
                'followed_id' => 1,
            ],
            [
                'follower_id' => 3,
                'followed_id' => 2,
            ],
            [
                'follower_id' => 3,
                'followed_id' => 1,
            ],
            [
                'follower_id' => 1,
                'followed_id' => 4,
            ],
            [
                'follower_id' => 2,
                'followed_id' => 5,
            ],
            [
                'follower_id' => 5,
                'followed_id' => 4,
            ],
            [
                'follower_id' => 1,
                'followed_id' => 5,
            ],
            [
                'follower_id' => 3,
                'followed_id' => 5,
            ],
        ];

        foreach ($followers as $follower) {
            if ($follower['follower_id'] !== $follower['followed_id']) {
                DB::table('followers')->insert($follower);
            }
        }

    }
}
