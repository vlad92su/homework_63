<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Comment;
use App\Models\Like;
use App\Models\Photo;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::factory()->count(10)->create();
        Photo::factory()->count(20)->create();
        Like::factory()->count(10)->create();
        Comment::factory()->count(10)->create();
        $this->call(FollowersTableSeeder::class);
    }
}
