$(document).ready(function () {
    $('.subscribe, .unsubscribe').click(function () {
        let button = $(this);
        let userId = button.data('user-id');
        let route = button.hasClass('unsubscribe') ? 'unsubscribe' : 'subscribe';

        $.ajax({
            type: 'POST',
            url: '/' + route + '/' + userId,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if (route === 'subscribe') {
                    button.text('отписаться');
                    button.removeClass('subscribe').addClass('unsubscribe');
                } else {
                    button.text('подписаться');
                    button.removeClass('unsubscribe').addClass('subscribe');
                }
            },
            error: function () {

            }
        });
    });


    $('.like-button').click(function () {
        let button = $(this);
        let photoId = button.data('photo-id');

        $.ajax({
            type: 'POST',
            url: '/like/' + photoId,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                console.log(response);
                if (response.action === 'like') {
                    button.html('<img style="width: 35px; height: 35px" src="' + response.imageUrl + '" alt="Лайк">');
                } else {
                    button.html('<img style="width: 35px; height: 35px" src="' + response.imageUrl + '" alt="Дизлайк">');
                }
                $('.like-count[data-photo-id="' + photoId + '"]').text(response.likes);
            },
            error: function () {

            }
        });

    });
});




