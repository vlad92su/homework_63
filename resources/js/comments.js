$(document).ready(function () {

    $('#create-comment-btn').click(function (event) {
        event.preventDefault();
        const data = $('#create-comment').serialize();
        const photoId = $('#photo_id').val();
        $.ajax({
            url: `/photos/${photoId}/comments`,
            method: "POST",
            data: data
        })
            .done(function (msg) {
                console.log('message => ', msg.comment);
                alert("на проверке");
                clearForm();
            })
            .fail(function (response) {
                console.log('FAIL RESPONSE =================> ', response);
            });
    });


    function renderData(comment, name) {

        let html = `<div class='card w-75 mb-3' id='delete-card-${comment.id}'>` +
            "<div class='card-body'>" +
            `<h5 class='card-title'><b>Author:</b> ${name}</h5>\n`;
        html += `<p class='card-text comment-text'><b>Comment:</b>${comment.comment}</p>\n` +
            `<textarea class="form-control comment-input mt-2" id="comment" name="comment" style="display: none;">${comment.comment}</textarea>` +
            `<button class="btn btn-primary delete-comment-btn mt-2" style="margin-right: 5px" data-id="${comment.id}">delete</button>` +
            `<button class="btn btn-primary update-comment-btn mt-2" data-value="${comment.id}">update</button>` +
            "</div>" +
            "</div>";

        $('#card-body').append(html);
        clearForm();
    }

    function clearForm() {
        $("#create-comment").trigger('reset');
    }


    $('#card-body').on('click', '.delete-comment-btn', function () {
        let commentId = $(this).data('id');
        deleteComment(commentId);
    });

    $('#card-body').on('click', '.update-comment-btn', function () {
        let card = $(this).closest(".card-body");

        if (card.find(".update-comment-btn").text() === "save") {
            const photoId = $('#photo_id').val();

            let commentId = $(this).data('value');
            let rating = card.find(".comment-rating-select").val();
            let comment = card.find(".comment-input").val();

            $.ajax({
                url: `/photos/${photoId}/comments/${commentId}`,
                method: "PUT",
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    commentId: commentId,
                    rating: rating,
                    comment: comment
                }
            })
                .done(function (msg) {
                    console.log('message => ', msg.comment);
                    card.find(".comment-rating").show();
                    card.find(".comment-rating").html("<b>Rating:</b> " + rating);
                    card.find(".comment-rating-select").hide();

                    card.find(".comment-text").show();
                    card.find(".comment-text").html("<b>Comment:</b> " + comment);
                    card.find(".comment-input").hide();

                    card.find(".update-comment-btn").text('update');

                })
                .fail(function (response) {
                    console.log('FAIL RESPONSE =================> ', response);
                });

        } else {
            card.find(".comment-rating").hide();
            card.find(".comment-rating-select").show();

            card.find(".comment-text").hide();
            card.find(".comment-input").show();

            card.find(".update-comment-btn").text('save');
        }
    });


    function deleteComment(id) {
        const photoId = $('#photo_id').val();

        $.ajax({
            url: `/photos/${photoId}/comments/${id}`,
            method: "DELETE",
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                id: id
            }
        })
            .done(function (msg) {
                console.log('message => ', msg);
                $(`#delete-card-${id}`).remove();
            })
            .fail(function (response) {
                console.log('FAIL RESPONSE =================> ', response);
            });

    }
});
