@extends('layouts.app')

@section('content')

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <div class="container">
        <div class="row">
            @foreach($users as $user)
                @if($user->id !== auth()->user()->id)
                    <div class="insta_card">
                        <div class="inf_user">
                            <div class="user_name"><img src="https://cdn-icons-png.flaticon.com/512/6596/6596121.png"
                                                        class="avatar" alt="">{{$user->name}}</div>
                            @if(auth()->user()->isFollowing($user))
                                <button data-user-id="{{ $user->id }}" class="unsubscribe">отписаться</button>
                            @else
                                <button data-user-id="{{ $user->id }}" class="subscribe">подписаться</button>
                            @endif
                        </div>


                        @foreach($user->photos as $photo)

                            <div class="photo_user">
                                <img src="{{ asset('/storage/' . $photo->photo)}}" class="d-block w-100" alt="...">
                            </div>
                            <div class="actions">
                                @if($photo->likedByUser(auth()->user()))
                                    <button class="like-button" data-photo-id="{{ $photo->id }}">
                                        <img src="{{asset('img/free-icon-heart-2589175.png')}}" alt=""
                                             style="width: 35px">
                                    </button>
                                @else
                                    <button class="like-button" data-photo-id="{{ $photo->id }}">
                                        <img src="{{asset('img/free-icon-heart-shape-outline-25424.png')}}" alt=""
                                             style="width: 35px">
                                    </button>
                                @endif
                                <span class="like-count"
                                      data-photo-id="{{ $photo->id }}">{{  $photo->likes()->count() }}</span>
                            </div>
                            <p class="d-inline-flex gap-1">
                                <button class="btn btn-primary" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseExample{{$photo->id}}"
                                        aria-expanded="false" aria-controls="collapseExample{{$photo->id}}">
                                    Show all comments
                                </button>
                            </p>

                            <div class="collapse" id="collapseExample{{$photo->id}}">
                                <div class="card card-body" id="card-body">
                                    @foreach($photo->comments as $comment)
                                        @csrf
                                        <div class="card w-75 mb-3" id="delete-card-{{$comment->id}}">
                                            <input value="{{$comment->id}}" type="hidden" class="form-control"
                                                   id="comment_id"
                                                   name="comment_id">
{{--                                            @if($photo_id === $comment->$photo_id)                                                  // проблема--}}
                                            <div class="card-body">
                                                <h5 class="card-title"><b>Author:</b>{{ $comment->user->name}}</h5>
                                                <p class="card-text comment-text">
                                                    <b>Comment:</b> {{ $comment->comment }}</p>
                                                <textarea class="form-control comment-input mt-2" id="comment"
                                                          name="comment"
                                                          style="display: none;">{{ $comment->comment }}</textarea>
                                                @can('delete', $comment)
                                                    <button class="btn btn-primary delete-comment-btn mt-2"
                                                            data-id="{{$comment->id}}">delete
                                                    </button>
                                                @endcan
                                                @can('update', $comment)
                                                    <button class="btn btn-primary update-comment-btn mt-2"
                                                            type="button"
                                                            data-value="{{$comment->id}}">
                                                        update
                                                    </button>
                                                @endcan
                                            </div>
{{--                                            @endif                                                                                  //конец проблемы--}}
                                        </div>
                                    @endforeach
                                </div>

                                @if($photo->user_id !== Auth::user()->id)
                                <div id="comment-form">
                                    <form id="create-comment">
                                        @csrf
                                        <input type="hidden" id="photo_id" value="{{$photo->id}}">
                                        <div class="form-group mt-2">
                                            <label for="comment">Comment</label>
                                            <textarea class="form-control" id="comment" name="comment"></textarea>
                                            @error('comment')
                                            <div class="alert alert-danger">{{$message}}</div>
                                            @enderror
                                        </div>
                                        <div class="text-center">
                                            <button id="create-comment-btn" type="submit"
                                                    class="mt-3 btn btn-outline-primary btn-sm btn-block">
                                                Add new comment
                                            </button>
                                        </div>
                                    </form>
                                </div>
                                    @endif
                            </div>
                            <div style="height: 40px"></div>
                        @endforeach
                        {{--                    @endif--}}
                    </div>
                @endif
            @endforeach
        </div>
    </div>
@endsection

