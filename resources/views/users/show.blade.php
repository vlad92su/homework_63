@extends('layouts.app')

@section('content')

    <div class="container body">
        <div class="row">
            <div class="col bg-over">
                <div class="card mb-3">
                    <div class="card-body">
                        <div class="d-flex" style="justify-content: space-between;">
                            <h5 class="card-title"><b>{{ $user->name}}</b></h5>
                            <div style="display: flex; gap: 10px">
                                <p><b>подписчики</b> {{ $user->followers()->count() }}</p>
                                <p><b>подписки</b> {{ $user->following()->count() }}</p>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col">

                        <div style="width: 90%">
                            <p class="d-inline-flex gap-1">
                                <button class="btn btn-primary" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseExample" aria-expanded="false"
                                        aria-controls="collapseExample">
                                    add photo
                                </button>
                            </p>
                            <div class="collapse" id="collapseExample">
                                <form method="post" action="{{ route('photos.store') }}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="user_id" id="user_id" value="{{$user->id}}">
                                    <div class="form-group">
                                        <label for="photo" class="form-label">Choose file</label>
                                        <input value="{{old('photo')}}" class="form-control" type="file" id="photo"
                                               name="photo">
                                        @error('photo')
                                        <div class="alert alert-danger">{{$message}}</div>
                                        @enderror
                                    </div>
                                    <div class="mt-2">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="uni-box">
                    @foreach($user->photos as $photo)
                        <div style="border: 1px solid #b1aaaa;">
                            <div class="card" style="width: 18rem; margin-bottom: 20px;">
                                <div class="img-box">
                                    <img src="{{ asset('/storage/' . $photo->photo)}}" alt="{{$photo->photo}}">
                                </div>

                                <div class="actions">
                                    @if($photo->likedByUser(auth()->user()))
                                        <button class="like-button" data-photo-id="{{ $photo->id }}">
                                            <img src="{{asset('img/free-icon-heart-2589175.png')}}" alt=""
                                                 style="width: 35px">
                                        </button>
                                    @else
                                        <button class="like-button" data-photo-id="{{ $photo->id }}">
                                            <img src="{{asset('img/free-icon-heart-shape-outline-25424.png')}}" alt=""
                                                 style="width: 35px">
                                        </button>
                                    @endif
                                    <span class="like-count"
                                          data-photo-id="{{ $photo->id }}">{{  $photo->likes()->count() }}</span>
                                </div>
                            </div>
                            <div style="width: 90%;    margin: auto;" class="pb-3">
                                <p class="d-inline-flex gap-1">
                                    <button class="btn btn-primary" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#collapseExample{{$photo->id}}"
                                            aria-expanded="false" aria-controls="collapseExample{{$photo->id}}">
                                        Show all comments
                                    </button>
                                    `
                                </p>

                                <div class="collapse" id="collapseExample{{$photo->id}}">
                                    <div class="card card-body" id="card-body">
                                        @foreach($photo->comments as $comment)
                                            @csrf
                                            <div class="card w-75 mb-3" id="delete-card-{{$comment->id}}">
                                                <input value="{{$comment->id}}" type="hidden" class="form-control"
                                                       id="comment_id"
                                                       name="comment_id">
                                                @dd($photo_id)
                                                @if($photo_id === $comment->$photo_id)
                                                    <div class="card-body">
                                                        <h5 class="card-title"><b>Author:</b>{{ $comment->user->name}}
                                                        </h5>
                                                        <p class="card-text comment-text">
                                                            <b>Comment:</b> {{ $comment->comment }}</p>
                                                        <textarea class="form-control comment-input mt-2" id="comment"
                                                                  name="comment"
                                                                  style="display: none;">{{ $comment->comment }}</textarea>
                                                        @can('delete', $comment)
                                                            <button class="btn btn-primary delete-comment-btn mt-2"
                                                                    data-id="{{$comment->id}}">delete
                                                            </button>
                                                        @endcan
                                                        @can('update', $comment)
                                                            <button class="btn btn-primary update-comment-btn mt-2"
                                                                    type="button"
                                                                    data-value="{{$comment->id}}">
                                                                update
                                                            </button>
                                                        @endcan
                                                    </div>
                                                @endif
                                            </div>
                                        @endforeach
                                    </div>
                                    @if($photo->user_id !== Auth::user()->id)
                                        <div id="comment-form">
                                            <form id="create-comment">
                                                @csrf
                                                <input type="hidden" id="photo_id" value="{{$photo->id}}">
                                                <div class="form-group mt-2">
                                                    <label for="comment">Comment</label>
                                                    <textarea class="form-control" id="comment"
                                                              name="comment"></textarea>
                                                    @error('comment')
                                                    <div class="alert alert-danger">{{$message}}</div>
                                                    @enderror
                                                </div>
                                                <div class="text-center">
                                                    <button id="create-comment-btn" type="submit"
                                                            class="mt-3 btn btn-outline-primary btn-sm btn-block">
                                                        Add new comment
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    @endif
                                </div>

                                <div class="">
                                    <form class="" method="post"
                                          action="{{ route('photos.destroy', ['photo' => $photo]) }}">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-danger">Remove</button>
                                    </form>
                                </div>
                            </div>


                        </div>

                    @endforeach
                </div>

            </div>

        </div>
    </div>
    </div>



@endsection
