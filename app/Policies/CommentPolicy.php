<?php

namespace App\Policies;

use App\Models\Comment;
use App\Models\Photo;
use App\Models\User;

class CommentPolicy
{
    /**
     * Create a new policy instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @param Photo $photo
     * @return bool
     */
    public function create(User $user, Photo $photo): bool
    {
        return $user->id == $photo->user_id;
    }


    /**
     * @param User $user
     * @param Comment $comment
     * @return bool
     */
    public function update(User $user, Comment $comment): bool
    {
        return $user->id == $comment->user_id;
    }


    /**
     * @param User $user
     * @param Comment $comment
     * @return bool
     */
    public function delete(User $user, Comment $comment): bool
    {
        return $user->id == $comment->user_id;
    }
}
