<?php

namespace App\Http\Controllers;

use App\Http\Requests\PhotoRequest;
use App\Models\Photo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PhotosController extends Controller
{

    /**
     * @param Request $request
     * @param $photoId
     * @return JsonResponse
     */
    public function like(Request $request, $photoId)
    {
        $photo = Photo::find($photoId);

        $user = auth()->user();
        $action = 'like';

        if ($photo->likedByUser($user)) {
            $photo->unlike($user);
            $action = 'unlike';
        } else {
            $photo->like($user);
        }

        $likesCount = $photo->likes()->count();
        $imageUrl = $photo->likedByUser($user) ? asset('img/free-icon-heart-2589175.png') : asset('img/free-icon-heart-shape-outline-25424.png');

        return response()->json(['action' => $action, 'likes' => $likesCount, 'imageUrl' => $imageUrl]);
    }


    /**
     * @param PhotoRequest $request
     * @return RedirectResponse
     */
    public function store(PhotoRequest $request)
    {
        $photo = new Photo($request->all());
        $img = data_get($request, 'photo');
        if ($img) {
            $path = $img->store('photo', 'public');
            $photo['photo'] = $path;
        }
        $photo->user_id = auth()->user()->id;
        $photo->save();
        return redirect()->route('users.show', auth()->user()->id)
            ->with('status', "Photo created successfully!!!");
    }


    /**
     * @param Photo $photo
     * @return RedirectResponse
     */
    public function destroy(Photo $photo)
    {
        $photo->delete();
        return redirect()->back()->with('status', "Photo successfully deleted!");
    }
}
