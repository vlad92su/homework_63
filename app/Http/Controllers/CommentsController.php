<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Photo;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentsController extends Controller
{


    /**
     * @param Request $request
     * @param Photo $photo
     * @return JsonResponse
     */
    public function store(Request $request, Photo $photo)
    {
        $request->validate([
            'comment' => 'required'
        ]);

        $comment = new Comment();
        $comment->user_id = Auth::user()->id;
        $comment->comment = $request->input('comment');
        $comment->photo_id = $photo->id;
        $comment->save();

        return response()->json(['comment' => $comment], 201);
    }


    /**
     * @param Request $request
     * @param Photo $photo
     * @return JsonResponse
     */
    public function update(Request $request, Photo $photo)
    {
        $request->validate([
            'comment' => 'required'
        ]);

        $comment = Comment::find($request->commentId);
        $comment->comment = $request->comment;
        $comment->photo_id = $photo->id;

        $comment->update();

        return response()->json(['comment' => $comment], 201);
    }


    /**
     * @param $photoId
     * @param $commentId
     * @return JsonResponse
     */
    public function destroy($photoId, $commentId)
    {
        $comment = Comment::find($commentId);
        if (!$comment) {
            return response()->json(['message' => 'Комментарий не найден'], 404);
        }
        $comment->delete();
        return response()->json(['message' => 'Комментарий успешно удален']);
    }
}
