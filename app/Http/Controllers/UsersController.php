<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UsersController extends Controller
{

    /**
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function index()
    {
        $users = User::all();
        return view('users.index', compact('users'));
    }



    public function show(User $user)
    {
        return view('users.show', compact('user'));
    }

    /**
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     */
    public function subscribe(Request $request, User $user)
    {
        auth()->user()->following()->attach($user->id);

        return response()->json(['message' => 'Подписка успешно добавлена']);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     */
    public function unsubscribe(Request $request, User $user): JsonResponse
    {
        auth()->user()->following()->detach($user->id);

        return response()->json(['message' => 'Подписка успешно удалена']);
    }


}
