<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Photo extends Model
{
    use HasFactory;

    protected $fillable = ['photo', 'user_id'];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return HasMany
     */
    public function likes(): HasMany
    {
        return $this->hasMany(Like::class);
    }

    /**
     * @return HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class)->where('approve', 1);
    }

    /**
     * @param User $user
     */
    public function like(User $user)
    {
        $this->likes()->create(['user_id' => $user->id]);
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function likedByUser(User $user)
    {
        return $this->likes()->where('user_id', $user->id)->exists();
    }


    /**
     * @param $user
     * @return bool
     */
    public function isLikedByUser($user)
    {
        return $this->likes()->where('user_id', $user->id)->exists();
    }



    /**
     * @param User $user
     */
    public function unlike(User $user)
    {
        $this->likes()->where('user_id', $user->id)->delete();
    }
}
