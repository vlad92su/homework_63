<?php
namespace App\Orchid\Layouts;
use App\Models\Comment;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;
class CommentListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'comments';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        $status = TD::make('approve');

        return [
            TD::make('id', 'Id'),
            TD::make('comment', 'Comment')
                ->render(function (Comment $comment) {
                    return Link::make($comment->comment)
                        ->route('platform.comments.edit', $comment);
                }),
            TD::make('approve', 'Approve')->render(function ($model) {
                return $model->approve ? 'Опубликовано' : 'В проверке';
            }),
        ];
    }
}
