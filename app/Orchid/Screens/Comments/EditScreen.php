<?php
namespace App\Orchid\Screens\Comments;
use App\Models\Comment;
use App\Models\Photo;
use App\Models\User;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Orchid\Support\Facades\Alert;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Upload;
use Orchid\Support\Facades\Layout;
use Orchid\Screen\Screen;


class EditScreen extends Screen
{
    private Comment $comment;
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(Comment $comment): iterable
    {
        $this->comment = $comment;
        return [
            'comment' => $comment
        ];
    }
    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Проверка/редактирование комментария';
    }
    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Approve comment')
                ->icon('icon-pencil')
                ->method('approve')
                ->canSee($this->comment->exists),
            Button::make('Update comment')
                ->icon('icon-note')
                ->method('update')
                ->canSee($this->comment->exists),
            Button::make('Remove comment')
                ->icon('icon-trash')
                ->method('remove')
                ->canSee($this->comment->exists),
        ];    }
    /**
     * @return iterable
     * @throws BindingResolutionException
     */
    public function layout(): iterable
    {
        return [
            Layout::rows([
                Input::make('comment.comment')
                    ->title('Comment')
                    ->placeholder('Attractive but mysterious title')
                    ->help('Specify a short descriptive title for this article.'),
                Relation::make('comment.user_id')
                    ->title('Author')
                    ->fromModel(User::class, 'name', 'id'),
                Relation::make('comment.photo_id')
                    ->title('Photo')
                    ->fromModel(Photo::class, 'photo', 'id'),
            ])
        ];
    }


    /**
     * @param Comment $comment
     * @param Request $request
     * @return RedirectResponse
     */
    public function approve(Comment $comment, Request $request)
    {
        $comment->approve = true;
        $comment->save();
        Alert::info('You have successfully approved an comment.');

        return redirect()->route('platform.comments.list');
    }

    /**
     * @param Comment $comment
     * @param Request $request
     * @return RedirectResponse
     */
    public function update(Comment $comment, Request $request)
    {
        $comment->fill($request->get('comment'))->save();
        Alert::info('You have successfully created an comment.');

        return redirect()->route('platform.comments.list');
    }


    /**
     * @param Comment $comment
     * @return RedirectResponse
     */
    public function remove(Comment $comment)
    {
        $comment->delete()
            ? Alert::info('You have successfully deleted the comment.')
            : Alert::warning('An error has occurred')
        ;

        return redirect()->route('platform.comments.list');
    }
}
