<?php

namespace App\Orchid\Screens\Comments;

use App\Models\Comment;
use App\Orchid\Layouts\CommentListLayout;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class ListScreen extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'comments' => Comment::paginate(20)
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Comments';
    }


    /**
     * @return iterable
     */
    public function commandBar(): iterable
    {
        return [];
    }


    /**
     * @return iterable
     */
    public function layout(): iterable
    {
        return [
            CommentListLayout::class
        ];
    }
}
