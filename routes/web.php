<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware( 'auth')->group(function () {
    Route::resource('/users', App\Http\Controllers\UsersController::class)->only('index', 'show');
    Route::resource('/photos', App\Http\Controllers\PhotosController::class)->only('store', 'destroy');

    Route::post('/subscribe/{user}', 'App\Http\Controllers\UsersController@subscribe')->name('subscribe');
    Route::post('/unsubscribe/{user}', 'App\Http\Controllers\UsersController@unsubscribe')->name('unsubscribe');

    Route::post('/like/{photo}', [App\Http\Controllers\PhotosController::class, 'like'])->name('like.photo');

    Route::resource('photos.comments', \App\Http\Controllers\CommentsController::class)->only(['store', 'update', 'destroy']);

});


